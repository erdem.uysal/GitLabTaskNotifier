package net.erdemuysal.gitlabtasknotifier.util


fun String.isValidURL(): Boolean {
    return matches(Regex("(https?)+://[^\\s]+\\.[^\\s]+"))
}

fun CharSequence.isValidURL(): Boolean {
    return toString().isValidURL()
}