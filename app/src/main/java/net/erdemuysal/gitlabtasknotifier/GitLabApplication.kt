package net.erdemuysal.gitlabtasknotifier

import android.app.Application
import android.support.v7.app.AppCompatDelegate

class GitLabApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}