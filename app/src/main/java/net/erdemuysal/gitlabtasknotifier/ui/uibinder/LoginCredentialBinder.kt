package net.erdemuysal.gitlabtasknotifier.ui.uibinder

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.view.View
import net.erdemuysal.gitlabtasknotifier.R
import net.erdemuysal.gitlabtasknotifier.model.LoginCredential
import net.erdemuysal.gitlabtasknotifier.util.isValidURL

@BindingAdapter("bind:onFocusChange")
fun onFocusChange(view: View, listener: (View, Boolean) -> Unit) {
    view.setOnFocusChangeListener(listener)
}

@BindingAdapter("bind:error")
fun setError(layout: TextInputLayout, errorMessage: Int?) {
    errorMessage?.let {
        layout.error = layout.resources.getString(it)
    } ?: run {
        layout.error = null
        layout.isErrorEnabled = false
    }
}

class LoginCredentialBinder(val data: LoginCredential, val loginComponentActions: LoginComponentActions) : BaseObservable() {
    interface LoginComponentActions {
        fun onLoginButtonClicked()
        fun onObtainTokenButtonClicked()
    }

    private var tokenInputFocused: Boolean = false

    @Bindable
    fun getObtainButtonVisibility(): Int {
        return if (shouldATokenBeTaken() && tokenInputFocused) View.VISIBLE else View.GONE
    }

    @Bindable
    fun getLoginButtonVisibility(): Int {
        return if (loginCanProceed()) View.VISIBLE else View.GONE
    }

    @Bindable
    fun getTokenError(): Int? {
        return if (isTokenNotBlank()) null else R.string.error_field_required
    }

    @Bindable
    fun getURLError(): Int? {
        return if (data.url.isValidURL()) null else R.string.url_is_not_valid
    }

    fun loginCanProceed(): Boolean {
        return data.url.isValidURL() && isTokenNotBlank()
    }

    private fun shouldATokenBeTaken(): Boolean {
        return data.url.isValidURL() && !isTokenNotBlank()
    }

    private fun isTokenNotBlank() = (data.token?.isNotBlank() == true)

    fun onInputChange(s: Editable?) {
        notifyChange()
    }

    fun onTokenFocusChange() = { _: View, hasFocus: Boolean ->
        tokenInputFocused = hasFocus
        notifyChange()
    }

    fun loginButtonClicked(v: View) {
        loginComponentActions.onLoginButtonClicked()
    }

    fun obtainTokenButtonClicked(v: View) {
        loginComponentActions.onObtainTokenButtonClicked()
    }
}
