package net.erdemuysal.gitlabtasknotifier.ui

import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView

abstract class TextWatcherAdapter : TextWatcher {
    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }
}

fun TextView.addAfterTextChangedListener(afterTextChanged: (s: Editable?) -> Unit) {
    addTextChangedListener(object : TextWatcherAdapter() {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged(s)
        }
    })
}

fun TextView.addBeforeTextChangedListener(beforeTextChanged: (s: CharSequence?, start: Int, count: Int, after: Int) -> Unit) {
    addTextChangedListener(object : TextWatcherAdapter() {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            beforeTextChanged(s, start, count, after)
        }
    })
}

fun TextView.addOnTextChangedListener(onTextChanged: (s: CharSequence?, start: Int, before: Int, count: Int) -> Unit) {
    addTextChangedListener(object : TextWatcherAdapter() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChanged(s, start, before, count)
        }
    })
}

